/*****************************************************************************
 * Проект:
 *      "phy_mapper"
 *
 * Описание:
 *      Макроопределения кодов ioctl команд модулю ядра "phy_mapper".
 *
 * Комментарии:
 *
 * Разработчик:
 *      Igor Bolshevikov <bolshevikov.igor@gmail.com>
 *
 * license:
 *      This program is free software; you can redistribute it and/or modify it
 *      under the terms of the GNU General Public License as published by the
 *      Free Software Foundation; either version 2 of the License, or (at your
 *      option) any later version.
 *
 ****************************************************************************/
#ifndef _PHY_MAPPER_SHARE_WITH_US_H_
#define _PHY_MAPPER_SHARE_WITH_US_H_

#include <linux/types.h>
#ifndef u32
#   define u32 __u32
#endif
#ifndef u64
#   define u64 __u64
#endif
/******************************************************************************
 * Типы
 *****************************************************************************/
struct mem_area_param
{
    size_t  offst;
    size_t  size;
};

/******************************************************************************
 * Макроопределения кодов ioctl команд
 *****************************************************************************/
/** магическое чиисло - идентификатор ioctl команды */
#define MAPPER_LKM_IOC_MAGIC        ('P')

/**
 * Получить от модуля абсолютное смещение в физ памяти выделенное в dts
 */
#define PHY_MAPPER_GET_PHY_OFFST    (_IOR(MAPPER_LKM_IOC_MAGIC, 1, void*))

/**
 * Сбросить данных кэши из указанной области UserSpace адресов
 */
#define PHY_MAPPER_FLUSH_CACHE      (_IOR(MAPPER_LKM_IOC_MAGIC, 2, void*))

/** максимальное количество ioctl команд */
#define PHY_MAPPER_LKM_IOC_MAXNR    (2)


#endif /* _PHY_MAPPER_SHARE_WITH_US_H_ */
