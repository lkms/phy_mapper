#ifndef _PHY_MAPPER_H_
#define _PHY_MAPPER_H_

#include <linux/bitops.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/types.h>

/******************************************************************************
 * Макроопределения специфичные для устройства
 *****************************************************************************/

/******************************************************************************
 * Макроопределения общих ресурсов
 *****************************************************************************/
/* имя модуля */
#define THIS_MODULE_NAME                    "phy_mapper"
/* максимальное количество поддерживаемых усторйств */
#define DEVICE_COUNT_MAX                    (2)
/*
 * базовый минорный номер символьного устройства. Нужен для выделения мажорного
 * номера
 */ 
#define BASE_MINOR_DEV_NUM                  (0)

/******************************************************************************
 * Вспомогательные макроопределения
 *****************************************************************************/
/* если установлено, то будет вывод различной отладочной информации */
#define ON_MSG_INFO 1

#define MSG_INFO(str,arg...)                                        \
    ({                                                              \
        if (ON_MSG_INFO)                                            \
            printk(KERN_INFO THIS_MODULE_NAME ": " str,##arg);      \
    })

#define MSG_WARN(str,arg...)                                        \
    ({                                                              \
        if (ON_MSG_INFO)                                            \
            printk(KERN_WARNING THIS_MODULE_NAME ": " str,##arg);   \
    })

#define MSG_ERR(str,arg...)                                         \
    ({                                                              \
        printk(KERN_ERR THIS_MODULE_NAME ": [%s,%d] "               \
            str,                                                    \
            __func__,                                               \
            __LINE__,                                               \
            ##arg                                                   \
            );                                                      \
    })

/**
 * Проверка некоторого числа на принадлежность к кодам ошибок
 */
#define _IS_ERR_VAL(arg) IS_ERR_VALUE(ERR_PTR(arg))

/******************************************************************************
 * Определения типов специфичных для драйвера
 *****************************************************************************/

/**
 * Структура устройства 
 */
struct drvr_dev
{
    struct cdev         char_device;
    dev_t               cdev_numbers;
    struct resource     reserved_mem_resource;
};

#endif /* _PHY_MAPPER_H_ */
