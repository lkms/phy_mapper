/*****************************************************************************
 * Проект:
 *      "phy_mapper"
 *
 * Описание:
 *      Топ-файл модуля ядра
 *
 * Комментарии:
 *
 * Разработчик:
 *      Igor Bolshevikov <bolshevikov.igor@gmail.com>
 *
 * license:
 *      This program is free software; you can redistribute it and/or modify it
 *      under the terms of the GNU General Public License as published by the
 *      Free Software Foundation; either version 2 of the License, or (at your
 *      option) any later version.
 *
 ****************************************************************************/
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/ioctl.h>
#include <linux/module.h>
#include <linux/mm.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/uaccess.h>
#include <linux/types.h>
#include <asm/atomic.h>
#include <asm/current.h>
#include <asm/cacheflush.h>


#include "phy_mapper.h"
#include "phy_mapper_share_with_us.h"

/******************************************************************************
 * Прототипы
 *****************************************************************************/
static int drv_probe(
    struct platform_device*     pdev
    );
static int drv_remove(
    struct platform_device*     pdev
    );
static int drv_open(
    struct inode*               inode,
    struct file*                filep
    );
static int drv_close(
    struct inode*               inode,
    struct file*                filep
    );
static long drv_ioctl(
    struct file*                file,
    unsigned int                cmd,
    unsigned long               arg
    );
static int drv_mmap(
    struct file*                filp,
    struct vm_area_struct*      vma
    );

/******************************************************************************
 * init resource
 *****************************************************************************/
// максимальное количество устройств обрабатываемых драйвером
static int device_count_max = DEVICE_COUNT_MAX;
// максимальное количество открытий файла
module_param(device_count_max, int, 0444);

struct mutex cdev_num_mutex;

static struct class* dev_class = NULL;

// определим имя соответствующего устройства в dts
static struct of_device_id drvr_ids[] =
{
    {
        .compatible = "phy_mapper"
    },
    { /* end of table */ }
};

// Информируем ядро о том какие устройства мы хотим найти в dts
MODULE_DEVICE_TABLE(of, drvr_ids);

// Заполним структуру драйвера устройства
static struct platform_driver drvr_pd =
{
    .probe              = drv_probe,
    .remove             = drv_remove,
    .driver             = 
        {
            .name               = THIS_MODULE_NAME,
            .owner              = THIS_MODULE,
            .of_match_table     = drvr_ids
        }
};

// Заполняем структуру файловых операций драйвера
static const struct file_operations drv_fops =
{
    .owner              = THIS_MODULE,
    .open               = drv_open,
    .release            = drv_close,
    .unlocked_ioctl     = drv_ioctl,
    .mmap               = drv_mmap
};

/******************************************************************************
 * Глобальные переменные модуля
 *****************************************************************************/
static int  cdev_major  = 0;
static int  cdev_minor  = 0;

/******************************************************************************
 *  Функция динамического выделения старшего/младшего номеров устройства.
 *  note:    функция присваивает major/minor номера внешним переменным
 *         
 *  Возвращает в сучае успеха функция возвращает 0, в члуча ошибки - ее код.
 *****************************************************************************/
static int register_number_device(
    void
    )
{
    int ret = 0;
    dev_t number_device;

    ret = alloc_chrdev_region(
        &number_device,
        BASE_MINOR_DEV_NUM,
        device_count_max,
        THIS_MODULE_NAME
        );
    cdev_major = MAJOR(number_device);
    cdev_minor = MINOR(number_device);

    if(ret)
    {
        MSG_ERR("Can not allocate char device major/minor numbers\n" );
        return ret;
    }

    MSG_INFO("Major number of charaster device = [%d]\n", cdev_major);
    return 0;
}

/******************************************************************************
 * Функция освобождения номеров устройства.
 *****************************************************************************/
static void unregister_number_device(
    void
    )
{
    unregister_chrdev_region(
        MKDEV(
            cdev_major,
            BASE_MINOR_DEV_NUM
            ),
        device_count_max
        );
    return;
}

/******************************************************************************
 *  @brief Функция регистрации символьного устройства.
 *         
 *  @param      dev указатель на структуру устройства;
 *
 *  @retval     в сучае успеха функция возвращает 0, в члуча ошибки - ее код.
 *****************************************************************************/
static int create_symbol_device(
    struct drvr_dev*   dev
    )
{
    int ret_value;
    cdev_init(
        &dev->char_device,
        &drv_fops
        );
    dev->char_device.owner  = THIS_MODULE;
    
    mutex_lock(&cdev_num_mutex);
    if(cdev_minor == device_count_max)
    {
        MSG_ERR(
            "An error occurred while initializing the device. The limit of "
            "simultaneously available devices is exceeded (%d). If you really "
            "need an additional device, unload the driver and load it with the "
            "key: device_count_max = X, where the desired maximum number of "
            "sdevices hould be transmitted by the parameter X\n",
            device_count_max
            );
        mutex_unlock(&cdev_num_mutex);
        return(-EPERM);
    }
    dev->cdev_numbers = MKDEV(cdev_major, cdev_minor);
    cdev_minor++;
    mutex_unlock(&cdev_num_mutex);

    ret_value = cdev_add(
        &dev->char_device,
        dev->cdev_numbers,
        1
        );
    if(ret_value)
    {
        mutex_lock(&cdev_num_mutex);
        cdev_minor--;
        dev->cdev_numbers = -1;
        mutex_unlock(&cdev_num_mutex);
        MSG_ERR("error create_symbol_device cdev_add\n");
        return(ret_value);
    }
    MSG_INFO("create symbol device is success.\n");
    return 0;
}

/******************************************************************************
 *  @brief Удаление символьного устрйоства
 *         
 *  @param      dev     указатель на структуру устройства
 *
 *  @param      указатель на структуру устройства
 *****************************************************************************/
static void delete_symbol_device(
    struct drvr_dev*  dev
    )
{
    cdev_del(
        &dev->char_device
        );
    mutex_lock(&cdev_num_mutex);
    cdev_minor--;
    dev->cdev_numbers = -1;
    mutex_unlock(&cdev_num_mutex);
    MSG_INFO("delete symbol device\n");
}

/******************************************************************************
 *  @brief Функция создания класса sysfs для нашего устройства
 *
 *  @retval 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int create_class_device(
    void
    )
{
    dev_class = class_create(
        THIS_MODULE,
        THIS_MODULE_NAME
        );

    if (dev_class == NULL || IS_ERR(dev_class))
    {
        MSG_ERR("Can't create %s class on sysfs!\n", THIS_MODULE_NAME);
        return(PTR_ERR(dev_class));
    }

    MSG_INFO("Created %s class on sysfs.", THIS_MODULE_NAME);
    return(0);
}

/******************************************************************************
 *  @brief Функция добавления устросйтва в класс sysfs
 *
 *  @param      dev     указатель на структуру устройства
 *  @param      pdev    указатель на структуру шины pdev
 *
 *  @retval 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int add_device_to_class(
    struct drvr_dev*            dev,
    struct platform_device*     pdev
    )
{
    struct device* device;
    device = NULL;
    device = device_create(
        dev_class,
        &pdev->dev,
        dev->cdev_numbers,
        NULL,
        "%s.%d",
        THIS_MODULE_NAME,
        MINOR(dev->cdev_numbers)
        );

    if (    device == NULL
         || IS_ERR(device)
       )
    {
        MSG_ERR(
            "Can't create device entry for device %s%d\n",
            THIS_MODULE_NAME,
            MINOR(dev->cdev_numbers)
            );
        return PTR_ERR(device);
    }
    MSG_INFO(
        "Device %s.%d created\n",
        THIS_MODULE_NAME,
        MINOR(dev->cdev_numbers)
        );
    return 0;
}

/******************************************************************************
 *  @brief Функция удаления класса sysfs
 *****************************************************************************/
static void destroy_class_device(
    void
    )
{
    if (dev_class && ( ! IS_ERR(dev_class)))
    {
        class_destroy(dev_class);
        MSG_INFO("class device was destroed\n");
    }
}

/******************************************************************************
 *  Реализация функции open.
 *         
 *  @inode:  ;
 *  @filep:  ;
 *
 *****************************************************************************/
static int drv_open(
    struct inode*       inode,
    struct file*        filep
    )
{
    struct drvr_dev*  dev = container_of(
        inode->i_cdev,
        struct drvr_dev,
        char_device
        );

    filep->private_data = dev;

    nonseekable_open(inode, filep);
    return(0);
}

/******************************************************************************
 *  @brief Реализация функции close.
 *         
 *  @param  inode   ;
 *  @param  filep   ;
 *
 *  @retval         .
 *****************************************************************************/
static int drv_close(
    struct inode*       inode,
    struct file*        filep
    )
{
    return(0);
}

/******************************************************************************
 *  @brief Сливание кэшей данных из области памяти US адресов
 *         
 *  @param  mem_area - указатель на структуру с параметрами области памяти кэши
 *                     которой надо слить
 *
 *  @retval 0 в случае успеха, <0 - код ошибки
 *****************************************************************************/
static int flush_data_cache(
    struct mem_area_param*  mem_area
    )
{
    size_t start = mem_area->offst;
    size_t end   =   mem_area->offst
                   + mem_area->size;
    struct vm_area_struct* vma = find_vma(
                                        current->mm,
                                        mem_area->offst
                                        );
    if(!vma)
    {
        MSG_ERR("Bad vma\n");
        return -EFAULT;
    }
    if(vma->vm_start > end)
    {
        MSG_ERR("Bad vma start pointer\n");
        return -EFAULT;
    }

    if (start < vma->vm_start)
    {
        start = vma->vm_start;
    }

    if (end > vma->vm_end)
    {
        end = vma->vm_end;
    }

    flush_cache_range(
        vma,
        start,
        end
        );

    flush_tlb_range(
        vma,
        start,
        end
        );

    return 0;
}

/******************************************************************************
 * @brief Реализация функции ioctl.
 *        
 * @param  file    ;
 * @param  cmd     ;
 * @param  arg     ;
 *
 * @retval      в случае успеха функция возвращает 0, в случае ошибки - ее код.
 *****************************************************************************/
static long drv_ioctl(
    struct file*    file,
    unsigned int    cmd,
    unsigned long   arg
    )
{
    int              ret_access = 0;
    int              ret_value  = 0;
    struct drvr_dev* dev        = (struct drvr_dev*)file->private_data;
    
    struct mem_area_param map_res;

    if(_IOC_TYPE(cmd) != MAPPER_LKM_IOC_MAGIC)
    {
        ret_value = -ENOTTY;
        goto ioctl_exit;
    }

    if(_IOC_NR(cmd) > PHY_MAPPER_LKM_IOC_MAXNR)
    {
        ret_value = -ENOTTY;
        goto ioctl_exit;
    }

    switch (cmd)
    {
        case PHY_MAPPER_GET_PHY_OFFST:
            ret_access = access_ok(
                (void __user *)arg,
                 _IOC_SIZE(cmd)
                );
            if( ! ret_access)
            {
                ret_value = -EFAULT;
                MSG_ERR("user address is bad :(\n");
                goto ioctl_exit;
            }

            map_res.offst = dev->reserved_mem_resource.start;
            map_res.size  =   dev->reserved_mem_resource.end
                            - dev->reserved_mem_resource.start
                            + 1;

            ret_value = copy_to_user(
                (void __user *)arg,
                &map_res,
                sizeof(struct mem_area_param)
                );
            break;
        case PHY_MAPPER_FLUSH_CACHE:
            ret_access = access_ok(
                (void __user *)arg,
                 _IOC_SIZE(cmd)
                );
            if( ! ret_access)
            {
                ret_value = -EFAULT;
                MSG_ERR("user address is bad :(\n");
                goto ioctl_exit;
            }

            ret_value = copy_from_user(
                &map_res,
                (void __user *)arg,
                sizeof(struct mem_area_param)
                );
            ret_value = flush_data_cache(
                &map_res
                );

            break;

        default:
            MSG_WARN("wrong ioctl cmd (cmd = %d)\n",cmd);
            ret_value = -ENOTTY;
    }

ioctl_exit:
    return(ret_value);
}

/******************************************************************************
 * @brief Отображение в US памяти
 *****************************************************************************/
static int drv_mmap(
                struct file*            filp,
                struct vm_area_struct*  vma
                )
{
    /* флаги отображения. Явно укажем не кэширвоать эту память */
    //vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
    vma->vm_page_prot = pgprot_dmacoherent(vma->vm_page_prot);
    /* размер отображения */
    u64 size = vma->vm_end - vma->vm_start;

    phys_addr_t offset = (phys_addr_t)vma->vm_pgoff << PAGE_SHIFT;

    if (offset >> PAGE_SHIFT != vma->vm_pgoff)
    {
        MSG_ERR("mmap: bad offst!\n");
        return -EINVAL;
    }

    if (offset + (phys_addr_t)size - 1 < offset)
    {
        MSG_ERR("mmap: wrap around to the end of the physical mem!\n");
        return -EINVAL;
    }

    int rc = remap_pfn_range(
                    vma,
                    vma->vm_start,
                    vma->vm_pgoff,
                    size,
                    vma->vm_page_prot
                    );
    if(0 != rc)
    {
        MSG_ERR("mmap: vse propalo!\n");
        return -EAGAIN;
    }
    return 0;
}

/******************************************************************************
 * @brief Поиск в dt memory ресурса, и его отображение
 *
 *        Ищет в dt ресур памяти, и если находит - отображает его на вертуальную
 *        память ядра
 *
 * @param pdev          : указатель на platform device;
 * @param phandle_name  : имя искомого ресурса;
 * @param resource      : указатель на возвращаемый указатель;
 *
 * @ret 0 в случае успешного разбора, код ошибки -EINVAL в случае если строка
 *      с именем не соответсвует ни одному из известных имен каналов IPI
 *****************************************************************************/
static int probe__get_dt_resource(
    struct platform_device*     pdev,
    const char*                 phandle_name,
    struct resource*            resource
    )
{
    int ret_value = 0;
    if(resource == NULL)
    {
        MSG_ERR("bad resource param\n");
        goto iommap_dt_resource_exit;
        ret_value = -EINVAL;
    }
    struct device_node* dev_node = of_parse_phandle(
                                            pdev->dev.of_node,
                                            phandle_name,
                                            0
                                            );
    if ( ! dev_node)
    {
        MSG_ERR("No %s specified\n", phandle_name);
        goto iommap_dt_resource_exit;
    }

    ret_value = of_address_to_resource(
                                dev_node,
                                0,
                                resource
                                );
    /* независимо от кода возврата освободим ресурс */
    of_node_put(dev_node);
    if (ret_value)
    {
        MSG_ERR("No memory address assigned to the region\n");
        goto iommap_dt_resource_exit;
    }


iommap_dt_resource_exit:
    return ret_value;
}

/******************************************************************************
 *  @brief Реализация функции probe
 *         
 *  @param      pdev    указатель на структуру шины pdev
 *
 *  @retval 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int drv_probe(
    struct platform_device*     pdev
    )
{
    int                 ret_value = -EBUSY;
    struct drvr_dev*    dev;

    /* выделение памяти под саму структуру */
    dev = devm_kzalloc(
        &pdev->dev,
        sizeof(struct drvr_dev),
        GFP_KERNEL
        );
    if(NULL == dev)
    {
        MSG_ERR("bad memory request for dev object\n");
        goto probe_exit;
    }

    /*
     * Вычитаем параметры блока памяти для отображения
     */
    ret_value = probe__get_dt_resource(
        pdev,
        "memory-region",
        &dev->reserved_mem_resource
        );
    if(_IS_ERR_VAL(ret_value))
    {
        MSG_ERR("bad resources request for reseved memory area\n");
        goto probe_exit_free_dev;
    }

    /*
     * зарегистрируем наше символьное устройство
     */
    ret_value = create_symbol_device(
        dev
        );
    if(ret_value)
    {
        goto probe_exit_free_dev;
    }

    /*
     * добавим устойство к классу в sysfs, что с автоматически создаст файл
     * устройства в /dev директории
     */
    ret_value = add_device_to_class(
        dev,
        pdev
        );
    if(ret_value)
    {
        delete_symbol_device(
            dev
            );
        goto probe_exit_free_dev;
    }

    /* сохраним указатель на структуру нашего устройства */
    platform_set_drvdata(
        pdev,
        (void*)dev
        );
    return(0);
probe_exit_free_dev:
    devm_kfree(
        &pdev->dev,
        dev
        );
probe_exit:
    MSG_ERR("probe of \"%s\" is failed :(\n", THIS_MODULE_NAME);
    return(ret_value);
}

/******************************************************************************
 *  @brief Реализация функции remove
 *
 *  @param      pdev    указатель на структуру шины pdev
 *
 *  @retval 0 в случае успеха, или код ошибки
 *****************************************************************************/
static int drv_remove(
    struct platform_device*     pdev
    )
{
    struct drvr_dev* dev = (struct drvr_dev*) platform_get_drvdata(
        pdev
        );
    device_destroy(
        dev_class,
        dev->cdev_numbers
        );
    delete_symbol_device(
        dev
        );
    MSG_INFO("remove exit\n");
    return 0;
}


/******************************************************************************
 * @brief Функция инициализации ресурсов вызывающаяся при загрузку модуля в ядро
 *****************************************************************************/
static int __init drvr_init(
    void
    )
{
    int     ret_value = 0;

    mutex_init(&cdev_num_mutex);

    ret_value = create_class_device();
    if(ret_value)
        goto exit_init;
    
     /*
      * выделим мажорный номер для устройства, и выдилим память для структур.
      * \note
      *     Память выделяется под некоторое максимальное количество устройств,
      *     при динамическом добавлении устройств их количество не должно
      *     превышать количество выделенное здесь
      */
    ret_value = register_number_device();
    if(ret_value)
    {
        goto err_register_numbdev;
    }

    // Зарегистрируем нашу платформу в ядре
    ret_value = platform_driver_register(
        &drvr_pd
        );
    if(ret_value)
    {
        MSG_ERR(
            "platform_driver_register returned %d\n",
            ret_value
            );
        goto err_platform_dev;
    }
    else
    {
        MSG_INFO("module successfully initialized!\n");
        goto exit_init;
    }

err_platform_dev:
    unregister_number_device();
err_register_numbdev:
    destroy_class_device();
exit_init:
    return(ret_value);
}

/******************************************************************************
 * @brief Функция очистки ресурсов при выгрузке модуля
 *****************************************************************************/
static void __exit drvr_exit(
    void
    )
{
    platform_driver_unregister(&drvr_pd);
    
    destroy_class_device();

    unregister_number_device();

    MSG_INFO("module successfully unregistered\n");
}

/*подсунем ядру функции инициализации/выхода нашего модуля*/
module_init(drvr_init);
module_exit(drvr_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Igor Bolshevikov <bolshevikov.igor@gmail.com>");
MODULE_DESCRIPTION("Phy memory mapper dirver");
MODULE_VERSION("1.0");
